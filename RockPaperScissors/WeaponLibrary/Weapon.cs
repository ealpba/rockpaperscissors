﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeaponLibrary
{
    public enum Weapon
    {
        Rock,
        Paper,
        Scissors,
        Unknown
    }
}
