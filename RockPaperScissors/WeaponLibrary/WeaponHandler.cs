﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeaponLibrary
{
    public class WeaponHandler
    {
      
        public void DisplayWeapons()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Press [1] for Rock");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Press [2] for Paper");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Press [3] for Scissors\n");
            Console.ForegroundColor = ConsoleColor.Green;
        }
        public bool ValidateWeapon(int weapon)
        {
            if (weapon == 1 || weapon == 2 || weapon == 3)
            {
                return true;
            }
            return false;
        }
    }
}
