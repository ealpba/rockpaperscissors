﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLibrary;
namespace ConsoleUI
{
    class Program
    {
        private static Game game = new Game();
        static void Main(string[] args)
        {
            try
            {
                ConsoleDecoration();
                int gameType = Convert.ToInt16(Console.ReadLine());
                StartChoosenGameType(gameType);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }
        static void ConsoleDecoration()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Title = "Rock-Paper-Scissors Game";
            Console.WriteLine("Welcome to Rock-Paper-Scissors Game \n \n");
            Console.WriteLine("Enter 1 For Playing <Player vs Player> ");
            Console.WriteLine("Enter 2 For Playing <Player vs PC> ");
            Console.WriteLine("Enter 3 For Playing <PC vs PC> \n");

        }
        /// <summary>
        /// Pre: 3 >= gameType > 0 
        /// Post: Game start
        /// </summary>
        /// <param name="gameType"></param>
        static void StartChoosenGameType(int gameType)
        {
            try
            {
                if (gameType == 1 || gameType == 2 || gameType == 3)
                {
                    switch (gameType)
                    {
                        case 1:
                            game.HumanVSHuman();
                            break;
                        case 2:
                            game.PlayervsPC();
                            break;
                        case 3:
                            game.PCvsPC();
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    Console.WriteLine($"{gameType} is not valid must be either | 1 | 2 | 3 |");
                    Console.WriteLine("Enter valid input ");
                    while (true)
                    {
                        try
                        {
                            int newValidInput = Convert.ToInt16(Console.ReadLine());
                            StartChoosenGameType(newValidInput);
                        }
                        catch (Exception)
                        {
                            throw new Exception("Something went wrong try againg");
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
