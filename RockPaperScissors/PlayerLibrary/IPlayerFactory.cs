﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerLibrary
{
    public interface IPlayerFactory
    {
        Player CreatePlayerObj(PlayerType playerType, string name);
    }
}
