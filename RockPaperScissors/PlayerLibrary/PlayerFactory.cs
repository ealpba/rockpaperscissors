﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerLibrary
{
    public class PlayerFactory : IPlayerFactory
    {
        public Player CreatePlayerObj(PlayerType playerType, string name)
        {
            Player ObjSelector = null;

            switch (playerType)
            {
                case PlayerType.Human:
                    ObjSelector = new Human(name);
                    break;
                case PlayerType.PC:
                    ObjSelector = new PC(name);
                    break;
                default:
                    break;
            }
            return ObjSelector;

        }
    }
}
