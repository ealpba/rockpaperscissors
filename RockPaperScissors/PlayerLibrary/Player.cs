﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeaponLibrary;
namespace PlayerLibrary
{
    public enum PlayerType
    {
        Human,
        PC
    }

    public class Player
    {
        private string _name;
        public string Name { get => _name; set => _name = value; }

        private int _roundWon = 0;
        public int RoundWon { get => _roundWon; set => _roundWon = value; }

        private int _weaponChoosed = 0;
        public int WeaponChoosed { get => _weaponChoosed; set => _weaponChoosed = value; }

        private Weapon _playerWeapon;
        public Weapon PlayerWeapon { get => _playerWeapon; set => _playerWeapon = value; }

        private string _winnerMessage;
        public string WinnerMessage { get => _winnerMessage; set => _winnerMessage = value; }

        private WeaponHandler weapon = new WeaponHandler();
        public Player(string name)
        {
            this._name = name;
        }
        public Player SetPlayerWeaponToEnum(int weaponNumber, Player player)
        {
            switch (weaponNumber)
            {
                case 1:
                    player.PlayerWeapon = Weapon.Rock;
                    break;
                case 2:
                    player.PlayerWeapon = Weapon.Paper;
                    break;
                case 3:
                    player.PlayerWeapon = Weapon.Scissors;
                    break;
                default:
                    player.PlayerWeapon = Weapon.Unknown;
                    break;
            }
            return player;
        }
        public Player ChooseHand(Player player)
        {
            short playerWeapon = 0;
            bool isWeaponValid = false;

            try
            {
                Console.WriteLine(player.Name + " Choose Weapon");
                playerWeapon = Convert.ToInt16(Console.ReadKey(true).KeyChar.ToString());
                isWeaponValid = weapon.ValidateWeapon(playerWeapon);
                if (isWeaponValid)
                {
                    player = SetPlayerWeaponToEnum(playerWeapon, player);
                }
                else
                {
                    Console.WriteLine("Invalid option choose between | 1 | 2 | 3 |");
                    playerWeapon = Convert.ToInt16(Console.ReadKey(true).KeyChar.ToString());
                    weapon.ValidateWeapon(playerWeapon);
                    isWeaponValid = weapon.ValidateWeapon(playerWeapon);
                    if (isWeaponValid)
                    {
                        player = SetPlayerWeaponToEnum(playerWeapon, player);
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return player;
        }
        public Player ChooseHandPC(Player player)
        {
            Random p1Weapon = new Random();
            int player1Weapon = p1Weapon.Next(1, 4);

            player = SetPlayerWeaponToEnum(player1Weapon, player);
            return player;
        }
    }
}
