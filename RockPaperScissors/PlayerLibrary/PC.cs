﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerLibrary
{
    public class PC : Player
    {
        private string _name;
        public PC(string name) : base(name)
        {
            this._name = name;
        }
    }
}
