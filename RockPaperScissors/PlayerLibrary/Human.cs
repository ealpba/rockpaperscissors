﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerLibrary
{
    public class Human : Player
    {
        private string _name;
        public Human(string name) : base(name)
        {
            this._name = name;
        }
    }
}
