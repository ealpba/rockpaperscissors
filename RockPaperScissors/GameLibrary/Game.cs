﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeaponLibrary;
using PlayerLibrary;

namespace GameLibrary
{
    public class Game
    {
        private PlayerFactory playerFactory;
        private Rule rule;
        private Player winner;
        private WeaponHandler weapon = new WeaponHandler();
        public int Rounds { get; set; }

        private Player FindWinner(Player player1, Player player2)
        {
            try
            {
                rule = new Rule();   
                winner = rule.FindWinner(player1, player2);
                return winner;
                
            }
            catch (Exception e)
            {
                throw new Exception("Could not find winner " + e.Message);
            }
        }
        public void HumanVSHuman()
        {
            Console.WriteLine("Human vs Human");

            playerFactory = new PlayerFactory();

            Console.WriteLine("Player 1: Insert your name");
            string player1Name = Console.ReadLine();


            Console.WriteLine("Player 2: Insert your name");
            string player2Name = Console.ReadLine();
            if (player1Name.Length < 1)
            {
                player1Name = "Player 1";
            }
            if (player2Name.Length < 1)
            {
                player2Name = "Player 2";
            }
            Player player1 = playerFactory.CreatePlayerObj(PlayerType.Human, player1Name);
            Player player2 = playerFactory.CreatePlayerObj(PlayerType.Human, player2Name);

            winner = BestOf5Rounds_HumanvsHuman(player1, player2);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"\nWinner best of 5 rounds: { winner.Name } \nWon: { winner.RoundWon }/{Rounds}");
            Console.Read();

        }
        public void PCvsPC()
        {
            Console.WriteLine("PC vs PC");

            playerFactory = new PlayerFactory();


            string player1Name = "Player 1";
            string player2Name = "Player 2";

            Player player1 = playerFactory.CreatePlayerObj(PlayerType.PC, player1Name);
            Player player2 = playerFactory.CreatePlayerObj(PlayerType.PC, player2Name);

            winner = BestOf5Rounds_PCvsPC(player1, player2);
            Console.ForegroundColor = ConsoleColor.Green;

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"\nWinner best of 5 rounds: { winner.Name } \nWon: { winner.RoundWon }/{Rounds}");
            Console.Read();

        }
        public void PlayervsPC()
        {
            Console.WriteLine("Under Construction");

        }
        private Player BestOf5Rounds_HumanvsHuman(Player player1, Player player2)
        {
            Rounds = 0;

            Console.Clear();
            weapon.DisplayWeapons();

            for (int i = 0; i < 5; i++)
            {
                if (player1.RoundWon >= 3 || player2.RoundWon >= 3)
                {
                    break;
                }

                try
                {
                    player1.ChooseHand(player1);
                    player2.ChooseHand(player2);
                    winner = FindWinner(player1, player2);
                    Rounds++;
                    if (winner != null)
                    {
                        winner.RoundWon++;

                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(winner.WinnerMessage + " Rounds Won: " + winner.RoundWon);
                        Console.ForegroundColor = ConsoleColor.Green;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("It's a tie");
                        Console.ForegroundColor = ConsoleColor.Green;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
            return winner;
        }
        private Player BestOf5Rounds_PCvsPC(Player player1, Player player2)
        {
            Random p1Weapon = new Random();
            Random p2Weapon = new Random();
            for (int i = 0; i < 5; i++)
            {

                if (player1.RoundWon >= 3 || player2.RoundWon >= 3)
                {
                    break;
                }

                player1.ChooseHandPC(player1);
                player2.ChooseHandPC(player2);

                Console.WriteLine($"Player 1: {player1.PlayerWeapon} Player 2: {player2.PlayerWeapon}");

                winner = FindWinner(player1, player2);
                if (winner == null)
                {
                    Console.WriteLine("Its a tie!");
                    continue;
                }
                winner.RoundWon++;

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine($"\n{winner.Name } wins with {winner.PlayerWeapon} \nWon: { winner.RoundWon }/{Rounds}");
                Console.ForegroundColor = ConsoleColor.Red;

            }
            return winner;

        }
        private Player BestOf5Rounds_PlayervsPC(Player player1, Player player2)
        {
            for (int i = 0; i < 5; i++)
            {

                if (player1.RoundWon >= 3 || player2.RoundWon >= 3)
                {
                    break;
                }

                Random p1Weapon = new Random();
                int player1Weapon = p1Weapon.Next(1, 4);

                Random p2Weapon = new Random();
                int player2Weapon = p2Weapon.Next(1, 4);

                player1.WeaponChoosed = player1Weapon;
                player2.WeaponChoosed = player2Weapon;
                Console.WriteLine("p1: " + player1.WeaponChoosed + " p2: " + player2.WeaponChoosed);


                player1.WeaponChoosed = player1Weapon;
                player2.WeaponChoosed = player2Weapon;

                winner = FindWinner(player1, player2);
                if (winner == null)
                {
                    Console.WriteLine("Its a tie!");
                    continue;
                }
                winner.RoundWon++;

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Winner: " + winner.Name + " wins with " + winner.WeaponChoosed + "[" + " Rounds Won: " + winner.RoundWon + " ]");
                Console.ForegroundColor = ConsoleColor.Red;

            }
            return winner;

        }
    }
}
