﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlayerLibrary;
using WeaponLibrary;
namespace GameLibrary
{
    public class Rule
    {
        // @pre  - 2 Player should have weapons
        // @post - Winner is found
        public Player FindWinner(Player player1, Player player2)
        {

            if (player1.PlayerWeapon == Weapon.Scissors && player2.PlayerWeapon == Weapon.Paper)
            {
                player1.WinnerMessage = $"{player1.Name} Wins with [{ player1.PlayerWeapon }] over [{ player2.PlayerWeapon }]";
                return player1;
            }
            else if (player1.PlayerWeapon == Weapon.Scissors && player2.PlayerWeapon == Weapon.Rock)
            {
                player2.WinnerMessage = $"{player2.Name} Wins with [{ player2.PlayerWeapon }] over [{ player1.PlayerWeapon }]";
                return player2;
            }
            else if (player1.PlayerWeapon == Weapon.Paper && player2.PlayerWeapon == Weapon.Rock)
            {
                player1.WinnerMessage = $"{player1.Name} Wins with [{ player1.PlayerWeapon }] over [{ player2.PlayerWeapon }]";
                return player1;
            }
            else if (player1.PlayerWeapon == Weapon.Paper && player2.PlayerWeapon == Weapon.Scissors)
            {
                player2.WinnerMessage = $"{player2.Name} Wins with [{ player2.PlayerWeapon }] over [{ player1.PlayerWeapon }]";
                return player2;
            }
            else if (player1.PlayerWeapon == Weapon.Rock && player2.PlayerWeapon == Weapon.Paper)
            {
                player2.WinnerMessage = $"{player2.Name} Wins with [{ player2.PlayerWeapon }] over [{ player1.PlayerWeapon }]";
                return player2;
            }
            else if (player1.PlayerWeapon == Weapon.Rock && player2.PlayerWeapon == Weapon.Scissors)
            {
                player1.WinnerMessage = $"{player1.Name} Wins with [{ player1.PlayerWeapon }] over [{ player2.PlayerWeapon }]";
                return player1;
            }
            else if (player1.PlayerWeapon == Weapon.Unknown && player2.PlayerWeapon == Weapon.Unknown)
            {
                return null;
            }
            else if (player1.PlayerWeapon == player2.PlayerWeapon)
            {
                return null;
            }
            return null;
        }
    }
}
